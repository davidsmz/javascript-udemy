

// Eventos
// Son acciones que hacen que se dispare una funcion


function evento(arg) {
    console.log("Se disperó el evento!!!");
    console.log(arg);
}



var obj = document.getElementById("objDemo");
console.log(obj);
obj.addEventListener("keypress", evento);




var objb = document.getElementById("objButton");
objb.addEventListener("focus", evento);


// bloquear el mouse derecho
// propiedad de arg.button, indica posicion de mouse
document.onmousedown = function (arg) {

    if (arg.button == 2) {
        alert("Click Bloqueado");
        return;
    }
    console.log("No hay problema");
    // console.log(arg);
    // alert("Click bloqueado")
}

// seleccionar texto en la ventana
document.onmouseup = function () {
    var txt = window.getSelection().toString();
    console.log(txt);
}


// Evento onsubmit
// Funcion para validar el submit
function validar() {
    // console.log("recibido!!");

    var name = document.getElementById("txtname").value;
    var lastname = document.getElementById("txtlastname").value;

    if (name.length < 1) {

        return false;
    }

    if (lastname.length < 1) {

        return false;
    }

    return true;
}

// Funcion para obtener dato de objeto de la URL

function getParamURL(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null
}



console.log(window.location.search);
console.log(getParamURL("txtlastname"));










