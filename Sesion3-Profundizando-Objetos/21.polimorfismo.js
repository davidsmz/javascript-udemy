// polimorfismo es una funcion que puede recibir diferentes tipos de datos

function determinaDato( a ){
    if( a === undefined ){
        console.log("No sé que hacer");
    }

    if (typeof a === "number") {
        console.log("A es un numero y puedo operar");
    }

    if (typeof a === "string") {
        console.log("A es un texto y puedo operar textos");
    }

    if (typeof a === "object") {
        console.log("A es un objeto");
        if (a instanceof Number) {
            console.log("Y es un objeto numérico");
        }
    }
}


var b = new Number(5); // cuando se define asi no es numero sino un objeto
console.log(b + 10);
determinaDato(b);























