
// Expresiones Regulares
// Las expresiones regulares son patrones utilizados para encontrar una 
// determinada combinación de caracteres dentro de una cadena de texto

// formas de declarar una expresion regular

// i: insensible a las mayusculas
// g: todas las apariciones
// m: multilinea
// var reg1 = new RegExp("a"); // declaracion de una expresion regular
// var reg2 = /^H/; // cuando H este al incio
// var reg2 = /o$/; // cuando o esta al final
// var reg2 = /.../; // 3 caracteres cuaquiera
// var reg2 = /^.o/; // primer caracter cualquiera y sigue o
// var reg2 = /[1-4]/; // para rangos se usa []
// var reg2 = /[a-j]/; // rango de letras entre la a y j
// var reg2 = /[a-jA-Z]/; // rango letras entre la a y j- A y Z
// var reg2 = /[aeiou].$/; // vocal, cualquier letra y final
// var reg2 = /\s/; // culaquier separacion de palabras
// var reg2 = /\w/; // equivalente a [a-zA-Z0-9]
// var reg2 = /\d/; // equivalente a [0-9]
// var reg2 = /m/i; // buscar m siendo insensible a la mayuscula
// var reg2 = /[aeiou]/ig; // todas las vocales siendo incase-sensitive
// var reg2 = /[aeiou]|é/ig; // la tuberia es como un o en condicional
// var reg2 = /o+/ig; // minimo 1 letra o y juntos
// var reg2 = /o?/ig; // aparezca o no la letra o, que haga mach
// var reg2 = /o*/ig; // que aparezca de 0 a mas letras o
// var reg2 = /o{2,}/ig; // que aparezca 2 veces a mas la letra o
var reg2 = /o{2,4}/ig; // que aparezca 2 veces a 4 la letra o





var texto = 'Hola 9 MUndoooooooooo\nHéllO wOrlD';
console.log(texto);
// match
// El método match() se usa para obtener todas las ocurrencias de una expresión 
// regular dentro de una cadena.
// Un método String que ejecuta una búsqueda por una coincidencia en una 
// cadena.Devuelve un array de información o null si no existe coincidencia 
// alguna

var arr = texto.match( reg2 );
console.log(arr);



var cadena = "La respuesta de la suma 60 + 45 = 105";
console.log(cadena);

// var reg3 = /[aeiou]{2,}/ig; // vocales juntas de 2 a más veces
// var reg3 = /\w{2}/ig; // 2 letras cualquiera juntas
// var reg3 = /\d{1,}/ig; // de 1 numero a más juntos
var reg3 = /\d{1,}|respuesta/ig; // de 1 numero a más juntos y luego rspta




var arr1 = cadena.match(reg3);
console.log(arr1);


























