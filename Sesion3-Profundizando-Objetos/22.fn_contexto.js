
// Analizando contexto de las funciones

// funcions que crea funciones

function crearFunciones1() {
    var arr = [];
    var numero = 1;

    arr.push(function () {
        console.log( numero );
    });

    numero = 2;

    arr.push(function () {
        console.log( numero );
    });

    numero = 3;

    arr.push(function () {
        console.log( numero );
    });


    return arr;


}


var funciones1 = crearFunciones1();

// javascript entra en el contexto de la funcion y al retornar arr lo hace con 
//el ultimo valor de numero
// en este caso el valor de numero es 3 por ello las 3 invocaciones devuelven 3 
// el valor del numero que es 3

funciones1[0](); // 3
funciones1[1](); // 3
funciones1[2](); // 3

// Veamos como modificamos esto con funciones anonimas

function crearFunciones2() {
    var arr = [];
    var numero = 1;

    arr.push(
        (function (numero) {

            return function () {
                console.log( numero );
            }

        })(numero)
    );

    numero = 2;

    arr.push(
        (function (numero) {

            return function () {
                console.log( numero );
            }

        })(numero)
    );

    numero = 3;

    arr.push(
        (function (numero) {

            return function () {
                console.log( numero );
            }

        })(numero)
    );


    return arr;


}



var funciones2 = crearFunciones2();

funciones2[0](); // 1
funciones2[1](); // 2
funciones2[2](); // 3




// con ciclos

function crearFunciones3() {
    var arr = [];

    for (var numero = 1; numero <= 4; numero++) {
        
        arr.push(
            (function (numero) {
    
                return function () {
                    console.log( numero );
                }
    
            })(numero)
        );
        
    }


    return arr;


}


var funciones3 = crearFunciones3();

funciones3[0](); // 1
funciones3[1](); // 2
funciones3[2](); // 3
funciones3[3](); // 4













