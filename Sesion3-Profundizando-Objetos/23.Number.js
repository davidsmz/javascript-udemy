
var a = 10;
var b = new Number(10);

console.log( a === b ); // false
console.log( a == b ); // true

var n1 = 10.565824;
// tofixed : redonde numero decimales indicados
console.log( n1.toFixed(2) ); // 10.56

// toString : convierte los numeros a string
console.log( n1.toString() ); // "10.565824"

// toPrecision : reduce el numero a la cantidad indicada, 
// contando la parte entera del decimal
console.log( n1.toPrecision(4) ); // 10.57

// valueof : 
console.log(b); // Number {10}
console.log( b.valueOf() ); // retorna su valor primitivo, 10


var c = new Number("20");
console.log(c); // Number {20}
console.log(c.valueOf()); // 20

























