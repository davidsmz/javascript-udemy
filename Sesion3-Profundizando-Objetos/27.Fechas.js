
var fecha = new Date(2018, 8, 6);

console.log(fecha); // Thu Sep 06 2018 00:00:00 GMT-0500

// El método getDate() devuelve el día del mes para la fecha especificada de 
// acuerdo con la hora local
fecha.setDate(15);

console.log(fecha); // Sat Sep 15 2018 00:00:00 GMT-0500



Date.prototype.sumarDias = function (dias) { 
    this.setDate(this.getDate() + dias);
    return this;
}

console.log(fecha.sumarDias(16)); // Mon Oct 01 2018 00:00:00 GMT-0500


Date.prototype.sumarYears = function (years) {
    this.setFullYear(this.getFullYear() + years);
    return this;
}

console.log(fecha.sumarYears(-5)); // Tue Oct 01 2013 00:00:00 GMT-0500


















