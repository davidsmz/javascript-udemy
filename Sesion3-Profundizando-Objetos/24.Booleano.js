
var a = new Boolean();   // false
var b = true;
var c = new Boolean("texto"); // true
var d = new Boolean(0); // false
var e = new Boolean(1); // true
var f = new Boolean( NaN ); // false
var g = new Boolean( null ); // false
var h = new Boolean( undefined ); // false
var i = new Boolean(""); // false

console.log( a ); // {[[PrimitiveValue]]: false} , valor por defecto
console.log( b ); // true
console.log( c ); // {[[PrimitiveValue]]: true}
console.log( d ); // {[[PrimitiveValue]]: false}
console.log( e ); // {[[PrimitiveValue]]: true}
console.log( f ); // {[[PrimitiveValue]]: false}
console.log( g ); // {[[PrimitiveValue]]: false}
console.log( h ); // {[[PrimitiveValue]]: false}
console.log( i ); // {[[PrimitiveValue]]: false}

































