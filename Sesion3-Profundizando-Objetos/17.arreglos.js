
// Los arreglos son colecciones de objetos

let arr = [5,4,3,2,1];

console.log( arr ); // [5,4,3,2,1]
console.log( arr[0], arr[3] ); // 5 2

// reverse
arr.reverse(); // invierte el arreglo
console.log( arr ); // [1,2,3,4,5]

// map, regresa un arreglo
arr = arr.map(function( elemento ){
    elemento *= elemento;
    return elemento;
});

console.log( arr ); // [1,4,16,25]

arr = arr.map( Math.sqrt );

console.log( arr ); // [1,2,3,4,5]

// join
// El método join([sepadador]) une todos los elementos de una matriz
// (o un objeto similar a una matriz) en una cadena y devuelve esta cadena.
let arr1 = arr.join() // convierte el arreglo en string con sus valores
console.log( arr1 ); // "1,2,3,4,5"

let arr2 = arr.join("-") // separa los valores por caracter indicado
console.log( arr2 ); // "1-2-3-4-5"


// split
// El método split() devuelve el nuevo array
// El método split() divide un objeto de tipo String en un array(vector) de 
// cadenas mediante la separación de la cadena en subcadenas.
let arr3 = arr2.split("-"); // inversa de join, corta elementos por signo indicado
console.log( arr3 ); // ["1","2","3","4","5"]

let arr4 = arr3.map(function( elem ){
    elem = parseInt(elem);
    return elem;
});

console.log( arr4 ); // [1,2,3,4,5]


// push
// El método push() agrega uno o más elementos al final de un array 
// y devuelve la nueva longitud del array.
arr.push(6); // ingresa un ultimo elemento del arreglo;
console.log( arr ); // [1,2,3,4,5,6]


// unshift
// El método unshift() agrega uno o más elementos al inicio del array, y 
// devuelve la nueva longitud del array.
arr.unshift(0); // ingresa un elemento al inicio del arreglo
console.log( arr ); // [0,1,2,3,4,5,6]


// toString
console.log( arr.toString() ); // "0,1,2,3,4,5,6"


// pop
// El método pop() elimina el último elemento de un array y lo devuelve.Este 
// método cambia la longitud del array.
let eliminado = arr.pop(); // se elimina el ultimo elemento y se guarda
console.log( arr, eliminado ); // [0,1,2,3,4,5] 6

// splice
// El método splice() cambia el contenido de un array eliminando elementos 
// existentes y / o agregando nuevos elementos.
// splice (param1, param2, r1, r2, r3 .....)
// param1 = indice desde cual se selecciona
// param2 = elementos borrados desde el indice param1
// r1, r2, r3, ... = elementos que entran en reemplazo
// retorna el arreglo quitado
let arreglo = [0,1,2,3,4,5,6];
console.log( arreglo );
arr5 = arreglo.splice(1, 2, 9, 10);
console.log( arreglo, arr5);



// slice
// El método slice() devuelve una copia de una parte del array dentro de 
// un nuevo array empezando por inicio hasta fin(fin no incluido).El array 
// original no se modificará.
// slice(param1, param2)
// param1 = indice desde el cual se selecciona
// param2 = indice hasta donde se realiza corte sin incluirlo

matriz = [0,1,2,3,4,5,6];
console.log( matriz );
arr6 = matriz.slice(1, 4);
console.log( matriz, arr6 );











