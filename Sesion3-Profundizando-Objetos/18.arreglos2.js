

var arr = [
    true,
    {
        nombre: "David",
        apellido: "Smz",
        edad: 23
    },
    function (){
        console.log("Funcion en un arreglo");
    },
    [
        "david", 
        "caleb", 
        "daniel", 
        "raquel",
        [
            "neil",
            "gladys",
            function (){
                console.log( this );
            }
        ]
    ]
];

console.log( arr );

function nombreCompleto( arreglo ){
    return arreglo[1].nombre + " " + arreglo[1].apellido;
}

console.log( nombreCompleto( arr ) ); // "David Smz"

console.log( arr[2]() ); // "Funcion en un arreglo"

console.log( arr[3][0] ); // "david"


console.log( arr[3][4][1] ) // "gladys"

var arr1 = arr[3][4];

arr1[1] = "elizabeth";

console.log( arr[3][4][1] );
console.log( arr );

// console.log( arr1[2]() );
arr1[2](); // ["neil", "elizabeth", ƒ]













