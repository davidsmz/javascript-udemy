// Sobrecarga de Operadores

/* 

function crearProducto(){

}

function crearProducto(nombre){

}

function crearProducto(nombre, precio){

}

*/

// javascript solo toma en cuenta la ultima declaracion de la funcion

// Solucion

function crearProducto( nombre, precio){
    nombre = nombre || "sin nombre";
    precio = precio || 0;

    console.log( "Producto: ", nombre, "|", "Precio: ", precio );

}


crearProducto("lapiz"); // Producto:  lapiz | Precio:  0

function crearProducto100(nombre){
    crearProducto(nombre, 100)
}

crearProducto100("regla"); // Producto:  regla | Precio:  100


function crearProductoCamisa(precio){
    crearProducto("camisa", precio);
}

crearProductoCamisa(500); // Producto:  camisa | Precio:  500































