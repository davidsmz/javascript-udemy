
var arguments = 10;

// arguments es array con los parametros enviados a la funcion
// este array se crea al momento de invocar a  la funcion

function miFuncion(a, b, c, d, e, f){
    console.log( arguments );
    // no imprime 10, porque existe arguments dentro del prototipo

}

miFuncion(10, 11, 12, {}, function(){});

function suma(a, b, c){
    if( arguments.length != 3 ){
        console.error("La funcion necesita 3 parametros");
        return;
    }
    console.log( a + b + c);
}

suma(1,2, 3); // 6


















