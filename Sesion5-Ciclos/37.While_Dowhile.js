
// while
var i = 0;

while (i <= 10) {
    i++;

    if (i == 7) {
        break; // salir del ciclo
    }

    if (i%2 == 0) {
        continue; // pasa al siguiente valor del ciclo
    }
    console.log(i);
}

console.log("============")

// Do-while

var j = 0;
do {
    j++;
    console.log(j);
} while (j < 5);

































