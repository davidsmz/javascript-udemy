
// Ciclo for

for (let i = 0; i < 10; i++) {

    if (i == 5) {
        continue;
    }

    if (i == 8) {
        break;
    }
    console.log(i)
    
}


// Ciclo For - in
// se utiliza para objetos
var Persona = function () {  
    this.nombre = "david";
    this.apellido = "smz";
    this.edad = 18;
}


var david = new Persona();
console.log(david)

Persona.prototype.direction = "Lima"

// imprime todas las propiedades del objeto incluso de prototype
for (const prop in david) {
    console.log(`${prop}: ${david[prop]}`);
}

console.log("==============================")

// imprime solo las propiedades dentro del objeto
// reflejo la propiedad de los objetos de conocerse a si mismo
for (const prop in david) {

    // hasOwnProperty, muestra si hay o no una propiedad del objeto
    // arroja un true si hay o  un false si no hay la porpiedad
    if (david.hasOwnProperty(prop)) {
        const element = david[prop];
        console.log(`${prop}: ${david[prop]}`);
    }
}

console.log("==============================")

// En un for in, en el array la prop es el index
var arr = [2, 3, 5, 7, 11, 13, 17, 19]
for (const i in arr) {
    console.log(i, ": ", arr[i])
}

console.log("==============================")

// Ciclo Foreach
// se utiliza para arrays
// cada parametro de la funcion es un elemento del array
arr.forEach(
    function (element) {  
        console.log(element)
    }
);




















