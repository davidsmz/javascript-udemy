
// setTimeout
// El método setTimeout() del mixin WindowOrWorkerGlobalScope establece un 
// temporizador que ejecuta una función o una porción de código después de que 
// transcurre un tiempo



// clearTimeout()
// El clearTimeout() método de WindowOrWorkerGlobalScopemixin cancela un tiempo 
// de espera establecido previamente llamando setTimeout().



setTimeout(() => {
    console.log("Paso un segundo");
}, 1000);



// El setInterval()método de la WindowOrWorkerGlobalScopemezcla llama 
// repetidamente a una función o ejecuta un fragmento de código, con un retraso 
// de tiempo fijo entre cada llamada.Devuelve un interval IDidentificador único 
// del intervalo, por lo que puede eliminarlo más tarde llamando clearInterval()
// Este método se ofrece en Windowe Workerinterfaces

var seg = 0;

var intervalo = setInterval(function() {

    seg++;
    console.log("segundo: " + seg);

    if (seg == 4) {
        clearInterval(intervalo);
    }
}, 1000);



var david = {
    nombre: "david",
    edad: 25,
    imprimir: function () {
        var self = this;
        setTimeout(function() {
            console.log(self);
            console.log(self.nombre, self.edad);
        }, 1000);
    }
}

david.imprimir()


// Paso un segundo
// segundo: 1
// { nombre: "david", edad: 25, imprimir: ƒ }
// david 25
// segundo: 2
// segundo: 3
// segundo: 4




















