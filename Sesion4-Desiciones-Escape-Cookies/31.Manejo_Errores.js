
// Manejo de errores en javascript

// throw hace que se ejecute el catch, va dentro del try
// throw puede enviar cualquier objeto o valor primitivo
// throw hace que todos los errores es envien a e directamnete

try {
    
    // Este error se utiliza si se puede contemplar el error
    // throw new Error('Error tipo 1'); // forma correcta de ejectar error
    throw {
        nombreError: 'Error tipo 1',
        acction: 'Salir corriendo',
        codeError: 1
    }

    console.log("Esta parte no se ejecuta");


} catch (e) {
    
    // console.log(e.message); // esto recide del new Error

    console.log(e);
    console.log(e.nombreError);
    console.log(e.acction);
    console.log(e.codeError);

    console.log('Parte del cath')

    regitroError(e.nombreError); // modo de registrar errores

} finally {
    console.log("Finalmenete");
}





function regitroError(e) {
    var now = new Date();
    console.log("Hubo un " + e + " a las " + now.getTime());
}




















