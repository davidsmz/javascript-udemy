

var objetoJS = {
    nombre: "david",
    edad: 25,
    imprimir: function () {
        console.log(`${this.nombre} - ${this.edad}`);
    }
}


console.log("Objeto: ", objetoJS); // Objeto:  {nombre: "david", edad: 25}

// Parsear un objeto
// Es convertir objeto a un json
// no convierte funciones dentro del objeto
var jsonString = JSON.stringify(objetoJS);

console.log(jsonString); // {"nombre":"david","edad":25}

// Convertir json a objeto
var jsonToObject = JSON.parse( jsonString );

console.log(jsonToObject); // {nombre: "david", edad: 25}
console.log(jsonToObject.nombre); // david
console.log(jsonToObject.edad); // 25
// console.log(jsonToObject.imprimir()); // no convertida la funcion

