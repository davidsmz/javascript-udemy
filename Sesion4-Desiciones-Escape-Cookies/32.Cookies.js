

// creacion de coockies
document.cookie = "usuario=david"

// borrar coockies
// se pone el valor expires una fecha antes de la actual
// document.cookie = "usuario=david; expires="fecha enterior a la actual UTC""


/* Funcion Crear Cookie */
function crearCookie(nombre, valor) {

    valor = escape(valor);
    // fecha
    var hoy = new Date();
    hoy.setMonth(hoy.getMonth() + 1)
    var fecha = hoy.toUTCString();
    // cookie
    document.cookie = `${nombre}=${valor}; expires=${fecha}`;

}


/* Funcion Borrar Cookie */
function borrarCookie(nombre) {
    // fecha
    var hoy = new Date();
    hoy.setMonth(hoy.getMonth() - 1)
    var fecha = hoy.toUTCString();
    // cookie
    document.cookie = `${nombre}=x; expires=${fecha}`;
}

/* Obtencion de coockies */
function getCookie(nombre) {

    let cookies = document.cookie;
    var cookieArr = cookies.split(" ");

    for (var i = 0; i < cookieArr.length; i++) {
        let parArr = cookieArr[i].split("=");
        cookieArr[i] = parArr;

        if (parArr[0] == nombre) {
            return unescape(parArr[1]);
        }
    }
    // console.log(cookieArr);

    return undefined;
}
// Creaciones de Coockies
crearCookie("nombre", "david");
crearCookie("apellido", "smz")
crearCookie("email", "davidsmz693@gmail.com");
crearCookie("direccion", "La Victoria, Lima; Peru");


// Borrar Coockies
borrarCookie("usuario");



// Mostrar las cookies
let cookies = document.cookie;
console.log(cookies);
console.log(getCookie("direccion"));

















