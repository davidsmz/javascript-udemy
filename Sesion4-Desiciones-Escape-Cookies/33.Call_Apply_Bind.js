
var carro = {
    color: "Negro",
    marca: "Ferrari",
    imrprimir: function () {
        var salida = this.marca + " - " + this.color;
        return salida;
    }
}


console.log(carro.imrprimir()); // Ferrari - Negro

var logCarro = function (arr1, arr2) {
    console.log("Carro: ", this.imrprimir());
    console.log("Argumentos: ", arguments)
    console.log("===========================")
}

// bind
// El método bind() crea una nueva función, que cuando es llamada, asigna a su 
// operador  this el valor entregado, con una secuencia de argumentos dados 
// precediendo a cualquiera entregados cuando la función es llamada
let logMarcaCarro = logCarro.bind(carro, "argss", "argst");
logMarcaCarro("args1", "args2"); 
// Carro:  Ferrari - Negro
// ["argss", "argst", "args1", "args2"]
// ===========================


// call
// El método call() llama a una función con un valor this asignado y argumentos 
// provistos de forma individual.
logCarro.call(carro, "args1", "args2");
// Carro:  Ferrari - Negro
// ["args1", "args2"]
// ===========================


// apply
// El método apply() invoca una determinada función asignando explícitamente el 
// objeto this y un array o similar(array like object) como parámetros
// (argumentos) para dicha función.
logCarro.apply(carro, ["arsg1", "args2"]);
// Carro:  Ferrari - Negro
// ["args1", "args2"]
// ===========================




// Funciones prestadas
var carro2 = {
    color: "Azul",
    marca: "Porsche",
}


console.log(carro.imrprimir.call(carro2));






