// Definicion de una funcion
// se utiliza el camelcase al nombrar funciones
// las funciones tienen scope
// si se utiliza una variable y no la encuentra la busca en objeto global


var b = 20;

function primeraFuncion(){
    var a = 10;
    console.log("a: ", a);
    console.log("b: ", b);
    console.log("c: ", c);
}


primeraFuncion();  // a: 10 , b: 20 , c: undefined

var c = 30;

primeraFuncion(); // a: 10 , b: 20 , c: 30

// Toda funcion retorna un valor, si no se indica es undefined

function funcionPrueba(){
    console.log("funcion prueba invocada")
}
var mifuncion = funcionPrueba(); // invoco a la funcion y ponemos su valor en una variable

console.log( mifuncion );








