
// Cuando se iguala variables se pasa sus valores por valor
// en este caso a y b tienen espacio de memoria diferentes
// y tienen valores diferentes
var a = 10;
var b = a;

console.log("a: ", a); // a:  10
console.log("b: ", b); // b:  10

a = 20;


console.log("a: ", a); // a:  20
console.log("b: ", b); // b:  10


// cuando se igualan los objetos se pasan sus valores por referencia
// En este caso obj1 y obj2 apuntan al mismo espacio de memoria

var obj1 = {
    nombre: "david",
    obj1Hijo: {
        nombreHijo: "raquel"
    }
}

var obj2 = obj1;

console.log("obj1: ", obj1);
console.log("obj2: ", obj2);

obj1.nombre = "daniel";


console.log("obj1: ", obj1);
console.log("obj2: ", obj2);


obj2.nombre = "caleb";


console.log("obj1: ", obj1);
console.log("obj2: ", obj2);

obj1.obj1Hijo.nombreHijo = "esdras";

// Esto hace cambiar nombreHijo en todas las consolas anteriores


