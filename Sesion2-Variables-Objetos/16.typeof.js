
/* ============ typeof ======================== */
// identifica que tipo de dato es el objeto dado

function identifica( param ){
    console.log( typeof param );
}


identifica( 1 );           // number
identifica( "texto" );     // string
identifica( true );        // boolean
identifica( {} );          // object

function nombre(){
    return 2;
}
identifica( nombre() );    // number
identifica( function(){} );// function

function Persona(){
    
}
var david = new Persona();
var caleb = new Persona();
identifica( david );       // object


// Instaceof se utiliza para ver si pertenece a un tipo de objeto
// retorna un true o false
function comparaObjeto( obj ){
    var comparacion = obj instanceof Persona;
    console.log( comparacion ); // compara si es el tipo de objeto
}

var daniel = {}
comparaObjeto( david );    // true
comparaObjeto( daniel )    // false




























