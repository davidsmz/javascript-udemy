// El uso de prototipos ayuda a la POO
// Cuando pides un metodo o propiedad busca en el objeto o su prototipo
function Persona( nombre, apellido, edad ){
    this.nombre = nombre;
    this.apellido = apellido;
    this.edad = edad;
}

Persona.prototype.datos = function (){
    return `${this.nombre} ${this.apellido} (${this.edad})`;
}

var david = new Persona("David", "Smz", 23);
console.log( david.datos() );

var caleb = new Persona("Caleb", "Smz", 2);
console.log( caleb.datos() );

// console.log( caleb.prototipoprueba );

// prototipo de un numero

Number.prototype.esPositivo = function (){
    if( this > 0){
        return true;
    }else{
        return false;
    }
}

var a = 1;
console.log(a.esPositivo()); // true
