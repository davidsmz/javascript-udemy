
// Javascript al entrar en el script lee todo el programa de un barrido
// y todos los objetos y variables los inicializa en undefined o null
// Luego empieza a leer linea por linea

console.log( variable1 ); // undefined

var variable1 = "David";

console.log( variable1 ); // "David"

var comparacion1 = undefined == null;
var comparacion2 = undefined === null;

console.log(comparacion1); // true
console.log(comparacion2); // false






