
/* =========== Funciones que retornan valores primitivos ========== */

// retorna un numero
function getAleatorio(){
    return Math.random();
}
console.log( getAleatorio() ); // 0.1564879564

// retorna un texto
function getNombre(){
    return "david";
}
console.log( getNombre() + " " + "smz" ); // david smz


function getLastName(){
    return "santa maria"
}

var lasName = getLastName();
console.log(getNombre() + " " + lasName); // david santa maria

// retorna un boleano
function esMayor05(){
    if( getAleatorio() > 0.5 ){
        return true;
    }else{
        return false;
    }
}

if( esMayor05() ){
    console.log("Es mayor a 0.5");
}else{
    console.log("Es menor a 0.5");
}


/* =========== Funciones que retornan objetos ========== */

function crearPersona( nombre, apellido ){
    return {
        nombre: nombre,
        apellido: apellido
    }
}

var persona = crearPersona("Melany", "Chavez");

console.log( persona.nombre + " " + persona.apellido );


/* =========== Funciones que retornan funciones ========== */

function crearFuncion(){
    return function( autor ){
        console.log("Creacion de una funcion por " + autor);
        return function (){
            console.log("Segunda funcion anonima");
        }
    }
}

var nuevaFuncion = crearFuncion();

nuevaFuncion("david y " + persona.nombre); // Creacion de una funcion por david y Melany

var segundaFuncion = nuevaFuncion("caleb"); // Se invoca a la funcion nuevaFuncion

segundaFuncion(); // Segunda funcion anonima





