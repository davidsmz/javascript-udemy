// Exsiten 2 tipos de datos, los primitivos y los objetos

/* ======================================== */
// Tipos de datos primitivos

var num = 10;
var str = "Texto";
var bol = true;
var und = undefined;
var nul = null;

// Javascript tiene escritura dinámica, las variables no estan amarradas
// a un tipo de dato en particular

num = str;

console.log(num);

bol = 10;

console.log(bol);

/* ======================================== */
// Objetos

// Obeto es una coleccion de datos primitivos o de ojetos
// Tiene nomenclatura de pares
// El ultimo par no tiene comas
var obj = {
    dato1: 10,
    dato2: "cadena de texto",
    dato3: false,
    objHijo: {
        dato1Hijo: 15,
        dato2Hijo: "cadena de texto 2"
    }
};

console.log(obj);