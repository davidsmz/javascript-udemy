/* ================================================= */
// Notacion del punto
// Nos permite tener los valores de un objeto
// Cada nivel jerarquico en el objeto es un punto

var persona = {
    nombre: "david",
    apellido: "smz",
    edad: 23,
    localizacion: {
        pais: "Perú",
        departamento: "Lima",
        distrito: "La Victoria",
        direccion: {
            urb: "Apolo",
            calle: "Jose Morales",
            numero: 120
        }
    }
};

console.log( persona.localizacion.departamento ); // "Lima"
console.log( persona.localizacion.direccion.urb ); // "Apolo"
console.log( persona.localizacion ); // objeto localizacion {}

console.log(persona.localizacion.direccion); // objeto direccion {}

// agregar una propiedad al objeto
// se agrega la propiedad piso a direccion

persona.localizacion.direccion.piso = 5;


console.log(persona.localizacion.direccion); // objeto direccion {}

// aplicando valor por referencia en los objetos

var direccion = persona.localizacion.direccion;

// se agrega la propiedad postal a direccion
direccion.postal = 15018;

console.log( direccion ); // objeto direccion {}

/* ================================================= */

// Notacion de Corchete

console.log( persona["nombre"] ); // "david"

var campo = "edad";

console.log( persona[campo] ); // 23











