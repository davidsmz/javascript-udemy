// Buena practica todas las funciones al inicio
// Los parametros son variables reservadas en la memoria
// pueden ser variables u objetos anonimos
// las variables pueden ser valores primitivos, objetos o funciones
// si no se envia el parametro en la invocacion, queda como undefined

/* ============== Parametro valores primitivos =================== */

function imprimir1( nombre, apellido ){
    console.log(nombre + " " + apellido);
}
// parametro variable anonima

imprimir1("david", "smz"); // "david smz"
imprimir1("caleb") // "caleb undefined"
imprimir1(); // "undefined undeined"

// parametro variable

var name1 = "Raquel";
var apell = "smz";

imprimir1(name1, apell);

console.log("\n");

/* ============== Parametro Objetos =================== */

function imprimir2( persona ){
    console.log(persona.name + " tiene " + persona.old + " años");
    persona.name = "caleb david";
}

// parametro objeto anonimo

imprimir2({
    name: "David",
    old: 23
});

// parametro con objeto

var obj = {
    name: "Caleb",
    old: 2
}

imprimir2( obj );

console.log(obj); // objeto {name: "caleb david", old: 2}

console.log("\n");

/* ============== Parametro Funciones =================== */

function imprimir3( fn ){
    fn();
}

// parametro funcion anonima

imprimir3( function (){
    console.log("Funcion anónima");
});

var miFuncion = function(){
    console.log("Otra funcion anonima");
}

imprimir3( miFuncion );













