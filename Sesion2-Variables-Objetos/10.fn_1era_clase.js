// Las funciones son objetos
// En JavaScript, las funciones son objetos de primera clase, es decir, 
// son objetos y se pueden manipular y transmitir al igual que cualquier 
// otro objeto. Concretamente son objetos Function
function a() {
  console.log("Funcion a");
}

a();

// agregando propiedades a las funciones
// A las funciones se le pueden agregar propiedade y metodos
a.nombre = "david";
a.localizacion = {
  pais: "Perú",
  ciudad: "Lima",
  distrito: "La Victoria",
  direccion: {
    calle: "José Morales",
    numero: 120,
    piso: "5to"
  }
}


console.log(a.nombre); // david
console.log(a.localizacion.ciudad); // Lima