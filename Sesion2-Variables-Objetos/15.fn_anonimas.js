
// funciones anonimas

var a = 50;
(function (){
    // Se crea un scope y no es afectada por variables globales
    var a = 10;
    console.log( a );
    function cambiarA(){
        a = 20;
    }
    
    cambiarA();
    console.log( a );

})(); // a: 10, a: 20

console.log( a ); // 50

function ejecutarFuncion( fn ){
    if( fn() > 0 ){
        return true;
    }else{
        return false;
    }
}

console.log(
    ejecutarFuncion(function (){
        console.log("Funcion anonima ejecutada");
        return 1;
    })
);

var valor = ejecutarFuncion(function (){
    console.log("Funcion anonima ejecutada");
    return 1;
});

if( valor ){
    console.log( "positivo" );
}else{
    console.log( "negativo" );
}













