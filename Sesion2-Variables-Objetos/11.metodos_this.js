// Los metodos son funciones dentro del objeto

var persona = {
    nombre: "David",
    apellido: "Smz",
    imprimirNombre: function (){
        console.log( this.nombre ); // this hace referencia al objeto mismo
    },
    direccion: {
        pais: "Perú",
        obtenerPais: function (){
            var self = this; // this hace referencia al objeto direccion
            
            var getDireccion = function (){
                // console.log( this ); hace referencia al objeto global
                console.log( "El pais es " + self.pais );
            }

            getDireccion();
        }
    }
}




persona.imprimirNombre(); // "David"
persona.direccion.obtenerPais(); // "El pais es Perú"

